#!/usr/bin/env bash

set -eu -o pipefail

function usage {
    cat <<EOF
Usage: $0 base-image
EOF
    exit 1
}

if [ $# -ne 1 ]; then
    usage
fi

base="$1"

tag=$(support/extract-image-tag "$base")
if [ -z "$tag" ]; then
    echo "Could not extract the tag from base image name: $base"
    exit 1
fi

mv_debug_image_name="${mv_debug_image_name:-docker-registry.discovery.wmnet/restricted/mediawiki-multiversion-debug}"
mv_debug_image_name="$mv_debug_image_name:$tag"

echo "** Building debug image: $mv_debug_image_name **"

cat <<EOF | docker build \
                   --build-arg "http_proxy=${http_proxy:-}" \
                   --build-arg "https_proxy=${https_proxy:-}" \
                   -t "$mv_debug_image_name" -f- empty/
FROM $base
USER root
RUN apt-get update && apt-get install -y php7.4-tideways php7.4-ldap
USER 33
EOF

echo "$mv_debug_image_name" > last-debug-build.tmp
mv last-debug-build.tmp last-debug-build

